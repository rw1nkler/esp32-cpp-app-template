#include "sdkconfig.h"
#include <esp_log.h>

static char tag[] = "cpp-app-template";

extern "C" {
  void app_main(void);
}

void app_main(void) {
  ESP_LOGI(tag, "C++ application template");
}
